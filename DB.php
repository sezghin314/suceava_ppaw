<?php

require_once __DIR__ . '/vendor/autoload.php';


use Doctrine\Common\Cache\ApcuCache;
use Doctrine\DBAL\Configuration;
use Monolog\Logger;
use MyApp\Daos\ContactDao;
use TheCodingMachine\TDBM\TDBMService;

$config = new Configuration();

$connectionParams = array(
    'user' => 'root',
    'password' => 'SuperPass',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
    'dbname' => 'suceava',
);



$dbConnection = Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

// The bean and DAO namespace that will be used to generate the beans and DAOs. These namespaces must be autoloadable from Composer.
$beanNamespace = 'MyApp\\Beans';
$daoNamespace = 'MyApp\\Daos';


$cache = new ApcuCache();
// die('hard'); 


$logger = new Logger('name'); // $logger must be a PSR-3 compliant logger (optional).

// Let's build the configuration object
$configuration = new TheCodingMachine\TDBM\Configuration(
    $beanNamespace,
    $daoNamespace,
    $dbConnection,
    null,    // An optional "naming strategy" if you want to change the way beans/DAOs are named
    $cache,
    null,    // An optional SchemaAnalyzer instance
    $logger, // An optional logger
    []       // A list of generator listeners to hook into code generation
);

// The TDBMService is created using the configuration object.
$tdbmService = new TDBMService($configuration);

$tdbmService->generateAllDaosAndBeans();
// Shazam! All PHP files have been written!

// die;
$userDao = new ContactDao($tdbmService);

$user = $userDao->getById(31);

var_dump($user->jsonSerialize());
die;
$mail = $user->getEmail();

$allUsers = $userDao->findAll();

foreach ($allUsers as $user) {
    var_dump($user->getPhone());
}
