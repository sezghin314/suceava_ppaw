CREATE TABLE `contacts` (
  `id` int(11) unsigned NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_pass` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `sip` varchar(255) DEFAULT NULL,
  `skype` varchar(255) DEFAULT NULL,
  `row_inserted` timestamp NOT NULL DEFAULT current_timestamp(),
  `row_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4