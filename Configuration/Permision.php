<?php

namespace Configuration;


interface Permision
{
    public const FUNCTION_WITHOUT_AUTH = [
        'get_all',
        'get_contact',
        'delete_contact',
        'update_contact',
        'insert_contact',
    ];

    public const DENIED_FUNCTIONS = [
        '__construct', 'initialize', 'run'
    ];
}
