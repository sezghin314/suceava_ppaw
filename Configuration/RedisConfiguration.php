<?php

namespace Configuration;

interface RedisConfiguration
{

    public const HOST = '127.0.0.1';

    public const PORT = 6379;

    public const TIMEOUT = 1;
}
