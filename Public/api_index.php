<?php
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers:Content-Type,Charset,Authorization,Access-Control-Request-Headers,Access-Control-Request-Method,Origin,X-Auth-Token,crossdomain");
header("Access-Control-Allow-Methods: POST, OPTIONS, GET");
header("Access-Control-Max-Age: 3600");

require_once __DIR__ . '/../vendor/autoload.php';

use DI\ContainerBuilder;
use App\Controllers\WebApiController;

$GLOBALS['start'] = microtime(true);
$containerBuilder = new ContainerBuilder();
// $containerBuilder->addDefinitions(__DIR__ . '/../Config/DIContainerConfiguration.php');
$container = $containerBuilder->build();
// echo 'build ' . (microtime(true) - $start) . PHP_EOL;
$start = microtime(true);
/**
 * @var  WebApiController $application
 */
$application = $container->get(WebApiController::class);

$application->initialize();

$application->run();

//echo 'end ' . (microtime(true) - $start) . PHP_EOL;
