<?php
header("Content-Type: application/json; charset=UTF-8");
// header("Content-Type: multipart/form-data; boundary=something");
header("Access-Control-Allow-Origin: *");
header(
    "Access-Control-Allow-Headers:Content-Type,Charset,Authorization,Access-Control-Request-Headers,Access-Control-Request-Method,Origin,X-Auth-Token,crossdomain");
header("Access-Control-Allow-Methods: POST, OPTIONS, GET");
header("Access-Control-Max-Age: 3600");
// declare (strict_types=1);

// require_once './../vendor/autoload.php';
require_once __DIR__ . '/../vendor/autoload.php';

$model = new Model();
$controller = new Controller($model);
$view = new View($controller, $model);

if (isset($_GET['action']) && !empty($_GET['action'])) {
$controller->{$_GET['action']}();
}

echo $view->output();