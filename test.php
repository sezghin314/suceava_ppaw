
<?php

use MyApp\Daos\ContactDao;
use TheCodingMachine\TDBM\TDBMService;

require_once __DIR__ . '/vendor/autoload.php';

$tdbmService = new TDBMService($configuration);
$userDao = new ContactDao($tdbmService);

$user = $userDao->getById(31);
echo $user->getEmail();