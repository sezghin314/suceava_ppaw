<?php

namespace App\Services;

use Configuration\RedisConfiguration;
use MyApp\Daos\ContactDao;


class ContactsService
{

    private $contactDao;

    // private $logger;

    // public function __construct(LoggerService $logger)
    public function __construct(
        ContactDao $contactDao

    ) {
        // $this->logger = $logger;
        $this->contactDao = $contactDao;
    }


    public function getContactById($args)
    {
        $id = $args['id'];
        $contact = $this->contactDao->getById($id);

        return $contact->jsonSerialize();
    }
}
