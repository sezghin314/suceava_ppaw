<?php

namespace App\Services;

use Configuration\RedisConfiguration;

class RedisService
{

    private $instance;

    // private $logger;

    public function __construct()
    {
        // $this->logger = $logger;
    }

    public function init()
    {
        try {
            $this->instance = new \Redis();
            if (!$this->instance->pconnect(RedisConfiguration::HOST, RedisConfiguration::PORT)) {
                exit();
            }
        } catch (\Exception $e) {
        }
    }

    public function getRedis()
    {
        if ($this->instance == null) {
            $this->init();
        }
        if ($this->instance->ping() === false) {
            $this->init();
        }
        return $this->instance;
    }

    public function __call($method, $parameters)
    {
        // echo "Вызов метода '$method' " . implode(', ', $parameters) . PHP_EOL;
        return call_user_func_array(array(
            $this->getRedis(),
            $method
        ), $parameters);
    }
}
