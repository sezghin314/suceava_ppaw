<?php

namespace App;

use App\Services\RedisService;

class HttpServer
{

    // private $logger;

    protected $redis;

    // public function __construct(LoggerService $logger, Arguments $args, RedisService $redis)
    public function __construct(RedisService $redis)
    {
        // $this->logger = $logger;
        // $this->args = $args;
        $this->redis = $redis;
    }


    /**
     * validates the type of method, limits the number of requests,go to "parse input"
     * @return array
     */
    public function requestProcessing(): array
    {

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'POST':
            case 'GET':
                return $this->getInput();
                break;
            case 'OPTIONS':
                http_response_code(200);
                echo json_encode([], JSON_UNESCAPED_UNICODE);
                exit();
                break;
            default:
                $this->reply(ResponseKeys::$WrongRequestMethod);
                break;
        }
    }

    /**
     *Create a response body, change the http code of the response, and reply to the client
     * @param mixed $response
     * @param $optionalData, optional arguments for replacement in translated message
     */
    public function reply($response, $optionalData = NULL)
    {
        /*
         * Verify that the response is default notification,
         * if so, in production exclude all messages leave only the codes.
         */
        if (isset($response['mess'])) {

            if ($response['code'] < 0) {
                $message = ResponseKeys::$BadGateway['mess'];
            } else {
                $message = $response['mess'];
                $code = $response['code'];
            }
        } else {

            $message = $response;
            $code = 0;
        }

        // default code is 0 (success)

        if ($code == ResponseKeys::$InvalidToken["code"] || $code == ResponseKeys::$TheTokenIsExpired["code"]) {
            http_response_code(401);
        } elseif ($code < 0) {
            http_response_code(406);
        } else {
            http_response_code(200);
        }

        $message = array(
            "code" => $code,
            "result" => $message
        );
        if ($optionalData) {
            $message['args'] = $optionalData;
        }

        echo json_encode($message, JSON_UNESCAPED_UNICODE);

        exit();
    }

    /**
     * Get input data, validate 
     * @return array 
     */
    public function getInput(): array
    {
        if (empty($_GET['action'])) {

            $this->reply(ResponseKeys::$ActionIsNotSet);
        }
        if (isset($_SERVER['HTTP_CONTENT_TYPE']) && preg_match('/multipart\/form-data;/', $_SERVER['HTTP_CONTENT_TYPE'])) {
            /*
             * Multimedia cannot be uploaded by php://inpup
             */
            $data = $_POST;
        } else {
            $data = ($_SERVER['REQUEST_METHOD'] == 'POST') ? json_decode(file_get_contents("php://input"), true) : array_map(
                'urldecode',
                $_GET
            );
        }

        return [
            $_GET['action'],
            $data,
            $_SERVER['HTTP_AUTHORIZATION'] ?? NULL
        ];
    }
}
