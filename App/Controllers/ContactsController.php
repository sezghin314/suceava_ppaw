<?php

namespace App\Controllers;

use App\HttpServer;
use App\ResponseKeys;
use App\Services\ContactsService;
use Configuration\Configuration;

class ContactsController extends AbstractController
{
    protected $httpServer;
    protected $contactS;
    // protected $contactDao;

    public function __construct(
        HttpServer $httpServer,
        ContactsService $contactS
    ) {
        $this->httpServer = $httpServer;
        $this->contactS = $contactS;
    }

    public function getContact($args)
    {
        $requiredFields = [
            'id'
        ];
        $this->checkIfArgsAreSet($args, $requiredFields);
        $response = $this->contactS->getContactById($args);
        // $mail = $response->();
        // $mail = $response->getEmail();

        // var_dump($response); die;
        $this->httpServer->reply($response);
        // $this->httpServer->reply($response);
    }


    public function setContact($args)
    {
        $requiredFields = [
            'id'
        ];
        $this->checkIfArgsAreSet($args, $requiredFields);
        $response = $this->contactS->get($args);
        // $mail = $response->();
        $mail = $response->getEmail();

        // var_dump($response); die;
        $this->httpServer->reply($mail);
        // $this->httpServer->reply($response);
    }
}
