<?php

namespace App\Controllers;

use App\Controllers\ContactsController;
use App\HttpServer;
use App\ResponseKeys;
use Configuration\Permision;

class WebApiController
{


    public const FUNCTION_WITHOUT_AUTH = [
        'get_all',
        'get_contact',
        'delete_contact',
        'update_contact',
        'insert_contact',
    ];

    public const DENIED_FUNCTIONS = [
        '__construct', 'initialize', 'run'
    ];


    // protected $authService;
    protected $httpServer;


    protected $contactC;


    public function __construct(
        HttpServer $httpServer,
        ContactsController $contactC
    ) {
        $this->httpServer = $httpServer;
        $this->contactC = $contactC;
    }

    /**
     * 
     *get action, arguments, and security token from the input
     * @return void
     */
    public function initialize()
    {

        list($this->action, $this->args) = $this->httpServer->requestProcessing();
        // list($this->action, $this->args, $this->token) = $this->httpServer->requestProcessing();
    }

    /**
     * Validate the action, 
     * verify that the action requires authentication, check security token
     * check that the same function exists in this controller, and call it
     * @return void
     */
    public function run(): void
    {

        if (in_array($this->action, self::DENIED_FUNCTIONS, true)) {
            exit();
        }

        if (!method_exists($this, $this->action)) {

            $this->httpServer->reply(ResponseKeys::$InvalidControllerName, ['args' => $this->action]);
            // echo 'Action not found';
        }
        var_dump('ssfgsdfbsdfbdbs');
        $this->{$this->action}();
    }


    public function get_contact()
    {
        // $this->checkPermission(Permision::);
        $this->contactC->getContact($this->args);
    }


    public function update_contact()
    {
        // $this->checkPermission(Permision::);
        $this->contactC->setContact($this->args);
    }
}
