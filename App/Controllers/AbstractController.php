<?php

namespace App\Controllers;

use App\ResponseKeys;

abstract class AbstractController
{

    // protected $user;

    // protected $session;

    // protected $role;


    /**
     *
     * @param array $requiredFields
     */
    protected function checkIfArgsAreSet($args, array $requiredFields)
    {
        if (!$this->checkIfRequiredFieldsAreSet($args, $requiredFields)) {

            $this->httpServer->reply(ResponseKeys::$LackOfArguments);
        }
    }

    /**
     *
     * @param array $requiredFields
     */
    protected function checkIfRequiredFieldsAreSet($args, array $requiredFields)
    {
        foreach ($requiredFields as $value) {
            if (empty($args[$value])) {
                return false;
            }
        }
        return true;
    }
}
