<?php

namespace App;

class ResponseKeys
{

    public static $Unaffected = [
        'mess' => 'unaffected',
        'code' => 2
    ];

    public static $Info = [
        'mess' => 'info',
        'code' => 1
    ];

    public static $Success = [
        'mess' => 'success',
        'code' => 0
    ];

    public static $Wargning = [
        'mess' => 'warning',
        'code' => -1
    ];

    public static $InvalidToken = [
        'mess' => 'invalid_token',
        'code' => -2
    ];

    public static $BadGateway = [
        'mess' => 'bad_gateway',
        'code' => -3
    ];

    public static $NoAccess = [
        'mess' => 'no_access',
        'code' => -4
    ];

    public static $NoAccessForAction = [
        'mess' => 'no_access_for_action',
        'code' => -5
    ];

    public static $NoAccessForController = [
        'mess' => 'no_access_for_controller',
        'code' => -6
    ];

    public static $LackOfArguments = [
        'mess' => 'lack_of_arguments',
        'code' => -7
    ];

    public static $WrongCredential = [
        'mess' => 'wrong_credential',
        'code' => -8
    ];

    public static $BadArguments = [
        'mess' => 'bad_arguments',
        'code' => -9
    ];

    public static $BadRequestMethod = [
        'mess' => 'bad_request_method',
        'code' => -10
    ];

    public static $WrongRequestMethod = [
        'mess' => 'wrong_request_method',
        'code' => -11
    ];

    public static $WrongArguments = [
        'mess' => 'wrong_arguments',
        'code' => -12
    ];
    public static $ActionIsNotSet = [
        'mess' => 'action_is_not_set',
        'code' => -13
    ];

    public static $EmptyToBeDeleted = [
        'mess' => 'empty_to_be_deleted',
        'code' => -14
    ];

    public static $WasNotDeleted = [
        'mess' => 'has_not_been_deleted',
        'code' => -15
    ];

    public static $NotInserted = [
        'mess' => 'not_inserted',
        'code' => -17
    ];

    public static $NotFound = [
        'mess' => 'not_found',
        'code' => -19
    ];

    public static $EmptyToBeUpdated = [
        'mess' => 'empty_to_be_updated',
        'code' => -20
    ];

    public static $NoHaveNecessaryRightsToChange = [
        'mess' => 'no_have_necessary_rights_to_change',
        'code' => -23
    ];

    public static $TheTokenIsExpired = [
        'mess' => 'expired_token',
        'code' => -24
    ];

    public static $Banned = [
        'mess' => 'banned',
        'code' => -27
    ];

    public static $NotAllowedExtension = [
        'mess' => 'not_allowed_extension',
        'code' => -28
    ];

    public static $DublicateRows = [
        'mess' => 'dublicate_rows',
        'code' => -32
    ];

    public static $InvalidControllerName = [
        'mess' => 'invalid_controller_name',
        'code' => -33
    ];
}
